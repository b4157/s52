function countLetter(letter, sentence) {
    
    let result = 0;

    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.
    
    let letterLength = letter.length
    
    if (letterLength == 1) {
        

        for (i = sentence.length; i >= 0; i--) {
        
            if (sentence[i] == letter) {
                result = result + 1;
            }
        }

        return result

    } else {

         return undefined
    }
    
}


function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.

    let text1 = text.toLowerCase()
    let text2 = text.toLowerCase()

    let iso=true

    for (let i=0; i < text1.length; i++) {
        
        // for (a = i+1; a >= 0; a++) {
            let a = i+1
            if (text1[i]==text2[a]) {
                iso = false
                break
                console.log(`${text1[i]} and ${text2[a]} `)
                
            } else {
                iso = true
                console.log(`${text1[i]} and ${text2[a]} `)
            }
        // }
    }

    // for (let i=0; i < text1.length; i++) {
        
    //     for (a = i-1; a >= 0; a--) {
            
    //         if (text1[i]==text2[a]) {
    //             iso = false
    //             // 'break
    //             console.log(`${text1[i]} and ${text2[a]} `)
                
    //         } else {
    //             iso = true
    //             console.log(`${text1[i]} and ${text2[a]} `)
    //         }
    //     }
    // }



    return iso
}

function purchase(age, price) {
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.
    let discountedPrice = price * 0.8
    let roundedDiscountedPrice = discountedPrice.toFixed(2)

    if(age<13){

        return undefined

    } else if(age>12 && age<22){
        
        return roundedDiscountedPrice
    
    } else if (age>21 && age<65){
    
        return price.toFixed(2)
    
    } else{
        return roundedDiscountedPrice
    }
}

function findHotCategories(items) {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.
    let hot=[]

    for (let i=0; i < items.length; i++) {
        
        if (items[i].stocks == 0) {
            hot.push(items[i]);
        }
    }

    return [...new Set(hot.map(item => item.category))];
}

function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']


    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    let voters=[]

    for (let i=0; i < candidateA.length; i++) {
        
        for (a = candidateA.length; a >= 0; a--) {
            if (candidateA[i]==candidateB[a]) {
                voters.push(candidateA[i])
            }
        }
    }

    return voters
    // return [...new Set(hot.map(item => item.category))];
}


module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};